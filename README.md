# Description

Minimal script to configure and install essential build tools on a mac.

It must be accompanied with another repository that contains the actual configuration.

```sh
curl -s https://bitbucket.org/yenashi/mac-bootstrap/raw/master/setup | bash -s
```

## Supported Flags

```sh
curl -s https://bitbucket.org/yenashi/mac-bootstrap/raw/master/setup | bash -s -- <flags>
```

Following is a list of installation options:
|        Name         | Flag |
| ------------------- | ---- |
| Multi-user Homebrew | \-m  |
| Ansible             | \-a  |

# Features

* It can be run many times without damage. Useful to reinstall broken components, like Command Line Tools after OS upgrade
* Enable TRIM (disabled by default on third-party SSDs)
* Enable Filevault (encryption key goes to the Desktop, in the file "FileVault Recovery Key.txt")
* Install Xcode Command Line Tools
* Upgrade [Python pip](https://pypi.org/project/pip/)
* Install [Homebrew](https://brew.sh/)
* Install [Ansible](https://github.com/ansible/ansible) (optional)
* Export PATH variable in user rc file (bash/zsh)

# Non Intrusive

The only features requiring sudo are TRIM and Filevault.

# Tested OS X Versions

* 10.15.x
